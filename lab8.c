//write your code here
#include <stdio.h>

int main()
{
     struct Date
        {
            int day,month,year;
        };
    struct EmployeeDetails
    {
        int Eid;
        char Ename[15];
        struct Date d;
        float salary;
    }e1;
   printf("Enter the Employee id\n");
   scanf("%d",&e1.Eid);
   printf("Enter the Employee name\n");
   scanf("%s",e1.Ename);
   printf("Enter the date of joining(Enter day month and year separetly)\n");
   scanf("%d%d%d",&e1.d.day,&e1.d.month,&e1.d.year);
   printf("Enter salary earned by the Employee\n");
   scanf("%f",&e1.salary);
    printf("EMPLOYEE DETAILS ARE:\nEmployee id: %d\nEmployee name: %s\nDate of joining:%d/%d/%d\nSalary: %f\n",e1.Eid, e1.Ename,e1.d.day,e1.d.month,e1.d.year,e1.salary);

    return 0;
}

