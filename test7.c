//write your code here
#include <stdio.h>

int main()
{
   printf("Enter the components of the matrix whoose transpose you would like to find\n");
   int g[3][2];
   for(int k=0;k<3;k++)
   {
      printf("Enter elements of row %d \n",(k+1));
      for(int n=0;n<2;n++)
      {
          scanf("%d",&g[k][n]);
          
      }
   }
   printf("Given Matrix-\n");
   for(int q=0;q<3;q++)//loop to print the given matrix
   {
       for(int w=0;w<2;w++)
       {printf("%d  ",g[q][w]);}
       printf("\n");
   }
   int tm[2][3];//2-D Array to store the transpose of the entered matrix
   printf("Transpose of the given matrix-\n");
   for(int i=0;i<2;i++)//loop to interchange the values of rows to column and vice versa
   {for(int j=0;j<3;j++)
   {tm[i][j]=g[j][i];//The index values of rows and columns of the two matrices are interchanged and therfore 
                     //the values of the rows of the entered matrix wil be stored in the columns of the 2-D array
                     //named tm[][] and vice versa
   printf("%d ",tm[i][j]);
       
   }
   printf("\n");
   }
   return 0;
}