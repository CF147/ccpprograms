//write your code here
#include <stdio.h>

int main()
{
    struct StudentDetail
    {
        int rn;
        char name[20];
        char sec[5];
        char department[10];
        float fee;
        float result;
    }s1,s2;
    printf("Enter the roll no. of both students\n");
    scanf("%d%d",&s1.rn,&s2.rn);
    printf("Enter the name of both students respectively\n");
    scanf("%s%s",s1.name,s2.name);
    printf("Enter their sections\n");
    scanf("%s%s",s1.sec,s2.sec);
    printf("Enter the departments they belong to\n");
    scanf("%s%s",s1.department,s2.department);
    printf("Enter the fees paid by each student\n");
    scanf("%f%f",&s1.fee,&s2.fee);
    printf("Enter the results of %s followed by %s\n",s1.name,s2.name);
    scanf("%f%f",&s1.result,&s2.result);
    if(s1.result>s2.result)
    {
     printf("The student details are:\nRoll no.: %d\nName: %s\nSection: %s\nDepartment: %s\nFee: %f\nResult: %f\n",s1.rn,s1.name,s1.sec,s1.department,s1.fee,s1.result);
    }
    else if(s1.result<s2.result)
    {
        printf("The student details are:\nRoll no.: %d\nName: %s\nSection: %s\nDepartment: %s\nFee: %f\nResult: %f\n",s2.rn,s2.name,s2.sec,s2.department,s2.fee,s2.result);
         
    }
    else 
    {printf("Both results are the same");}
   
    return 0;
}

