//write your code here
#include <stdio.h>

int main()
{
    int x,y;
    int *p,*q;
    p=&x,q=&y;
    printf("Enter two integers\n");
    scanf("%d%d",p,q);
    printf("%d + %d =%d\n",*p,*q,(*p+*q));
    printf("%d - %d =%d\n",*p,*q,(*p-*q));
    printf("%d * %d =%d\n",*p,*q,(*p**q));
    printf("%d / %d =%f\n",*p,*q,(*p /(float)*q));
    printf("%d %% %d =%d",*p,*q,(*p%*q));
    return 0;
}