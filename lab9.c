//write your code here
#include <stdio.h>
void swap(int *a1, int *a2)
{
    int t;
    t=*a1;
    *a1=*a2;
    *a2=t;
}
int main()
{
    int n1,n2;
    printf("Enter the integers to be swapped\n");
    scanf("%d%d",&n1,&n2);
    printf("Elements before swapping element 1=%d and element 2=%d\n",n1,n2);
    swap(&n1,&n2);
    printf("Elements after swapping element 1=%d and element 2=%d",n1,n2);
    return 0;
}
   