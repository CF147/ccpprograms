//write your code here
#include <stdio.h>

int main()
{
    FILE *fp;
    char ch;
    printf("Enter contents of the file\n");
    fp=fopen("INPUT","w");
    while((ch=getchar())!=EOF)
    {
        putc(ch,fp);
    }
    fclose(fp);
    printf("\nThe contents of the file are:\n");
    fp= fopen("INPUT","r");
    while(ch=getc(fp)!=EOF)
    {
        printf("%c",ch);
    }
    fclose(fp);
    

    return 0;
}
